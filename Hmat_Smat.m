function [H_mat, S_mat, basis_functions] = Hmat_Smat(a, b, lambda, m)    
    % Calculate the integrals of H_mat = <i|H|j> and S_mat = <i|j>
    
    syms x
    H = @(f) -1/2 * diff(f, 2) - lambda * exp(-x/a) * f;
    A = zeros(1, m);
    basis_functions = cell(1, m);
    H_mat = zeros(m, m);
    S_mat = H_mat;
    
    % calculate normalised values for A and generate basis function handles
    % to return
    for i = 1:m
        A(i) = sqrt(1/int((x^i * exp(-x/b))^2, 0, inf));
        basis_functions(i) = {@(y) A(i) .* y.^i .* exp(-y./b)};
    end
    
    % fill out H_ji
    for j = 1:m
        bra = A(j) * x^j * exp(-x/b);
        for i = 1:m
            ket = A(i) * x^i * exp(-x/b);
            H_mat(j, i) = int(bra * H(ket), 0, inf);
        end
    end
    
    % fill out S_ji
    for j = 1:m
        bra = A(j) * x^j * exp(-x/b);
        for i = j:m
            ket = A(i) * x^i * exp(-x/b);
            S_mat(j, i) = int(bra * ket, 0, inf);
            S_mat(i, j) = S_mat(j, i);
        end
    end
end

