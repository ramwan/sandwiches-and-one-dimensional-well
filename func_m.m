function [ground_state, first_excited, boundary, energies, bases, coefficients, ground_index] =...
func_m(m, b, well_boundary)
    % Values defined in the problem
    a = 2; lambda = 2;

    %
    % H = -1/2 *diff(_, 2) - lambda * exp(-x/a)
    % phi_m = A * x^m * exp(-x/b)
    %
    % returned H = <i|H|j>, integrated from 0 to inf
    % returned S = <i|j>, integrated from 0 to inf
    [H, S, basis_functions] = Hmat_Smat(a, b, lambda, m); % columns are eigenvectors
    [coefficients, energies] = eig(H, S);
    
    boundary = -lambda.*exp(-1.*well_boundary./a);
    bases = zeros(length(well_boundary), m);
    for i = 1:m
        tmp = basis_functions{i}(well_boundary);
        bases(:, i) = -abs(tmp);
    end

    ground_state = zeros(1, length(well_boundary));
    first_excited = zeros(1, length(well_boundary));
    [~, ground_index] = min(min(energies));
    for j = 1:m
        ground_state = ground_state + coefficients(j, ground_index) *...
                            basis_functions{j}(well_boundary);
        first_excited = first_excited + coefficients(j, 2) *...
                            basis_functions{j}(well_boundary);
    end
    ground_state(:);
    first_excited(:);
end
