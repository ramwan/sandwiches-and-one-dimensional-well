% question 2
% NOTE: first excited state extraction right now only works for b = 1.

clear; close ALL;
well_boundary = linspace(0, 10, 1000);
well_boundary = well_boundary(2:end);

% User input
% start_m: value of m to start at for phi basis functions
% length_m: number of m's to go through
%
b_or_m = "b"; % are we looking to adapt b or m?
%
start_m = 7;
length_m = 1;
start_b = 0.1;
start_b_plus_what = 1.5;
steps_b = 8;

if b_or_m == "m"
    fprintf(1, "Running for m, range %d to %d.\n", start_m, start_m + length_m);
    
    % we only have 2 bound states here
    ground_states = zeros(length(well_boundary), length_m);
    first_excited_states = zeros(length(well_boundary), length_m);

    i = 1;
    desc = cell(1, length_m - start_m + 3);
    desc{1} = "Well";
    for m = start_m:(start_m - 1)+length_m
        fprintf(1, "doing m: %d\n", m);
        [ground_states(:, i), first_excited_states(:, i), boundary, energies, bases, coeffs, ground_index] =...
            func_m(m, start_b, well_boundary);
        desc{i+1} = sprintf("Ground State: %d, Energy: %.5f", i, energies(ground_index, ground_index));
        i = i + 1;
    end
else
    fprintf(1, "Running for b, range %.2f to %.2f for %d steps\n", ...
        start_b, start_b+start_b_plus_what, steps_b);
    
    % we only have 2 bound states here
    ground_states = zeros(length(well_boundary), steps_b);
    first_excited_states = zeros(length(well_boundary), steps_b);

    i = 1;
    desc = cell(1, steps_b + 1);
    desc{1} = "Well";
    for b = linspace(start_b, start_b + start_b_plus_what, steps_b)
        fprintf(1, "doing b: %.2f\n", b);
        [ground_states(:, i), first_excited_states(:, i), boundary, energies, bases, coeffs, ground_index] =...
            func_m(start_m, b, well_boundary);
        desc{i+1} = sprintf("Ground State: %d, b: %.2f", i, b);
        i = i + 1;
    end   
end

fm = figure(1); grid; grid minor; hold on;
plot(well_boundary, boundary)
plot(well_boundary, bases, '--')
xlabel("x")
ylabel("energy - eV")

fm = figure(2); grid; grid minor; hold on;
plot(well_boundary, boundary)
plot(well_boundary, -abs(ground_states), '--')
legend(desc)
xlabel("x")
ylabel("energy - eV")